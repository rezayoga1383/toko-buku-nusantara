<?php

namespace App\Http\Controllers;

use App\Models\Buku;
use App\Models\KategoriBuku;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $buku = Buku::join('kategori_buku', 'buku.id_kategori_buku', '=', 'kategori_buku.id')
                ->select('buku.*', 'kategori_buku.nama_kategori')
                ->get();

        return view('admin.buku', compact('buku'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $kategori_buku = KategoriBuku::all();
        return view('admin.tambahbuku', compact('kategori_buku'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $request->validate([
            'nama_buku' => 'required',
            'gambar_buku' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'id_kategori_buku' => 'required',
            'penerbit' => 'required',
            'pengarang' => 'required',
            'deskripsi' => 'required',
            'tanggal_terbit' => 'required|date',
            'stok' => 'required|integer',
            'harga' => 'required|numeric',
        ],[
            'nama_buku.required' => 'Nama buku wajib diisi',
            'gambar_buku.required' => 'Gambar buku wajib diisi',
            'gambar_buku.image' => 'File harus berupa gambar',
            'gambar_buku.mimes' => 'Gambar harus berupa file dengan format: jpeg, png, jpg, gif, svg',
            'gambar_buku.max' => 'Ukuran gambar tidak boleh lebih dari 2MB',
            'id_kategori_buku.required' => 'Kategori wajib diisi',
            'penerbit.required' => 'Penerbit buku wajib diisi',
            'pengarang.required' => 'Pengarang wajib diisi',
            'deskripsi.required' => 'Deskripsi wajib diisi',
            'tanggal_terbit.required' => 'Tanggal Terbit wajib diisi',
            'stok.required' => 'Stok wajib diisi',
            'harga.required' => 'Harga wajib diisi',
        ]);

        // dd($request->all());

        $gambar = $request->file('gambar_buku');
        $filename = date('Y-m-d').$gambar->getClientOriginalName();
        $path = 'gambar-buku/'.$filename;

        Storage::disk('public')->put($path,file_get_contents($gambar));
        
        $data = [
            'nama_buku'         => $request->nama_buku,
            'gambar_buku'       => $filename,
            'id_kategori_buku'  => $request->id_kategori_buku,
            'penerbit'          => $request->penerbit,
            'pengarang'         => $request->pengarang,
            'deskripsi'         => $request->deskripsi,
            'tanggal_terbit'    => $request->tanggal_terbit,
            'stok'              => $request->stok,
            'harga'             => $request->harga,
        ];

        // dd($data);
        Buku::create($data);
        return redirect()->route('buku')->with('success', 'Kategori buku berhasil ditambahkan.');
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $buku = Buku::findOrFail($id);
        $kategori_buku = KategoriBuku::all(); // Asumsi Anda memiliki model KategoriBuku untuk mendapatkan data kategori buku

        return view('admin.editbuku', compact('buku', 'kategori_buku'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'nama_buku' => 'required',
            'gambar_buku' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'id_kategori_buku' => 'required',
            'penerbit' => 'required',
            'pengarang' => 'required',
            'deskripsi' => 'required',
            'tanggal_terbit' => 'required|date',
            'stok' => 'required|integer',
            'harga' => 'required|numeric',
        ]);
    
        $buku = Buku::findOrFail($id);
    
        if ($request->hasFile('gambar_buku')) {
            $gambar = $request->file('gambar_buku');
            $filename = date('Y-m-d').$gambar->getClientOriginalName();
            $path = 'gambar-buku/'.$filename;
    
            Storage::disk('public')->put($path, file_get_contents($gambar));
    
            // Hapus gambar lama jika ada
            if ($buku->gambar_buku) {
                Storage::disk('public')->delete('gambar-buku/' . $buku->gambar_buku);
            }
    
            $buku->gambar_buku = $filename;
        }
    
        $buku->nama_buku = $request->nama_buku;
        $buku->id_kategori_buku = $request->id_kategori_buku;
        $buku->penerbit = $request->penerbit;
        $buku->pengarang = $request->pengarang;
        $buku->deskripsi = $request->deskripsi;
        $buku->tanggal_terbit = $request->tanggal_terbit;
        $buku->stok = $request->stok;
        $buku->harga = $request->harga;
        $buku->save();
    
        return redirect()->route('buku')->with('success', 'Buku berhasil diperbarui.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $buku = Buku::findOrFail($id);
    
        // Hapus gambar buku dari penyimpanan jika ada
        if ($buku->gambar_buku) {
            Storage::disk('public')->delete('gambar-buku/' . $buku->gambar_buku);
        }

        $buku->delete();

        return redirect()->route('buku')->with('success', 'Buku berhasil dihapus.');
        }
}
