<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $totalKategori = DB::table('kategori_buku')->count();
        return view('/admin/dashboard', compact('totalKategori'));
    }
}
