<?php

namespace App\Http\Controllers;

use App\Models\Pengguna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function login()
    {
        return view('login');
    }

    public function autentikasi(Request $request)
    {
        // dd($request->all());
        Session::flash('email', $request->email);
        Session::flash('password', $request->password);

        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ], [
            'email.required' => 'email wajib diisi',
            'password.required' => 'password wajib diisi',
        ]);

        $infologin = [
            'email' => $request->email,
            'password' => $request->password
        ];

        // dd($infologin);
        // $pengguna = Pengguna::all();
        // dd($pengguna);
        // Cek kredensial pengguna
        if (Auth::attempt($infologin)){
            if (Auth::user()->role == "Admin"){
                return redirect()->route('dashboard-admin');
            }else{
                return redirect()->route('beranda-user');
            }
        }else{
            return redirect()->route('login')->with('notvalid','email dan password yang dimasukkan tidak sesuai');
        } 
                
    }

    public function admin()
    {
        return view('admin.dashboard');
    }

    public function pelanggan()
    {
        return view('user.home');
    }
}
