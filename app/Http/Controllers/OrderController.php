<?php

namespace App\Http\Controllers;

use App\Models\Buku;
use App\Models\Order;
use App\Models\Pengguna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function index()
    {
        return view('user.order');
    }

    public function checkout(Request $request)
    {
        // dd($request->all());
        $request->request->add(['total_bayar' => $request->jumlah * 100000, 'status' => 'Belum Dibayar']);
        // dd($request->all());
        $order = Order::create($request->all());

        // Set your Merchant Server Key
        \Midtrans\Config::$serverKey = config('midtrans.server_key');
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = false;
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = true;
        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = true;

        $params = array(
            'transaction_details' => array(
                'order_id' => $order->id,
                'gross_amount' => $order->total_bayar,
            ),
            'customer_details' => array(
                'nama' => $request->nama,
                'no_telepon' => $request->no_telepon,
            ),
        );

        $snapToken = \Midtrans\Snap::getSnapToken($params);
        // dd($snapToken);
        return view('user.checkout', compact('snapToken', 'order'));
    }
}
