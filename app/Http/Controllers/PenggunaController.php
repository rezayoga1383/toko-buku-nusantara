<?php

namespace App\Http\Controllers;

use App\Models\Pengguna;
use Illuminate\Http\Request;

class PenggunaController extends Controller
{
    public function index()
    {
        $pengguna = Pengguna::all();
        return view('admin.pengguna', compact('pengguna'));
        // return view('admin.pengguna');
    }

    public function create()
    {
        return view('admin.createpengguna');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'password' => 'required',
            'role' => 'required',
            'status' => 'required',
        ],[
            'nama' => 'Nama Wajib Diisi',
            'email' => 'Email Wajib Diisi',
            'password' => 'Password Wajib Diisi',
            'role' => 'Role Wajib Diisi',
            'status' => 'Status Wajib Diisi',
        ]);

        // dd($request['nama_kategori']);

        $pengguna = new Pengguna;
        $pengguna->nama = $request->nama;
        $pengguna->email = $request->email;
        $pengguna->password = $request->password;
        $pengguna->role = $request->role;
        $pengguna->status = $request->status;
        $pengguna->save();

        return redirect()->route('pengguna')->with('success', 'Pengguna berhasil ditambahkan.');
    }

    public function edit(string $id)
    {
        $pengguna = Pengguna::findOrFail($id);
        return view('admin.editpengguna', compact('pengguna'));
    }

    public function update(Request $request, string $id)
    {
        $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'role' => 'required',
            'status' => 'required',
        ],[
            'nama' => 'Nama Wajib Diisi',
            'email' => 'Email Wajib Diisi',
            'role' => 'Role Wajib Diisi',
            'status' => 'Status Wajib Diisi',
        ]);

        // dd($request['nama_kategori']);

        $pengguna = Pengguna::find($id);

        $pengguna->nama = $request->nama;
        $pengguna->email = $request->email;
        $pengguna->role = $request->role;
        $pengguna->status = $request->status;

        // Check if password is filled
        if ($request->filled('password')) {
            $pengguna->password = bcrypt($request->password);
        }

        $pengguna->save();

        return redirect()->route('pengguna')->with('success', 'Pengguna berhasil diubah.');
    }

    public function destroy(string $id)
    {
        $pengguna = Pengguna::findOrFail($id);
        $pengguna->delete();

        return redirect()->route('pengguna')->with('success', 'Pengguna berhasil dihapus');
    }

    public function admin()
    {
        return view ('admin.dashboard');
    }
}
