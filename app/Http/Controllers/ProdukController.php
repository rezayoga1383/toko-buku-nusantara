<?php

namespace App\Http\Controllers;

use App\Models\Buku;
use App\Models\KategoriBuku;
use Illuminate\Http\Request;

class ProdukController extends Controller
{
    public function index()
    {
        $kategori = KategoriBuku::all();
        $buku = Buku::all();
        return view('user.produk', compact('buku', 'kategori'));
    }
}
