<?php

namespace App\Http\Controllers;

use App\Models\Buku;
use Illuminate\Http\Request;

class ProdukDetailController extends Controller
{
    public function index($id)
    {
        $buku = Buku::findOrFail($id);
        return view('user.produkdetail', compact('buku'));
    }
}
