<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    use HasFactory;

    protected $table = 'buku';

    protected $fillable = [
        'id_kategori_buku',
        'nama_buku',
        'penerbit',
        'gambar_buku',
        'stok',
        'harga',
        'deskripsi',
        'tanggal_terbit',
        'pengarang',
    ];
    public $timestamps = true;

    public function kategori()
    {
        return $this->belongsTo(KategoriBuku::class, 'id_kategori_buku');
    }
}
