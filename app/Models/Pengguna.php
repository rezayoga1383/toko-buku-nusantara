<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Pengguna extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;

    protected $table = 'pengguna';

    protected $fillable = [
        'nama',
        'email',
        'password',
        'role',
        'status',
    ];
    
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    // Menyembunyikan password dan remember token
    protected $hidden = [
        'password',
        'remember_token',
    ];

    // Jika tabel menggunakan kolom timestamps (created_at dan updated_at)
    // set $timestamps ke true, jika tidak hapus atau set ke false
    public $timestamps = false; // Sesuaikan jika tabel Anda tidak menggunakan timestamps
}

