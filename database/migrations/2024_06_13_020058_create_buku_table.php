<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('buku', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_kategori_buku');
            $table->string('nama_buku');
            $table->string('penerbit');
            $table->string('pengarang');
            $table->string('gambar_buku');
            $table->text('deskripsi');
            $table->date('tanggal_terbit');
            $table->integer('stok');
            $table->decimal('harga', 10, 0); 
            $table->timestamps();

            $table->foreign('id_kategori_buku')->references('id')->on('kategori_buku')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('buku');
    }
};
