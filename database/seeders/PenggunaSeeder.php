<?php

namespace Database\Seeders;

use App\Models\Pengguna;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PenggunaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Pengguna::create([
            'nama' => 'Guru Gembul',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin'),
            'role' => 'Admin',
            'status' => 'Aktif',
        ]);
        Pengguna::create([
            'nama' => 'Reza',
            'email' => 'reza@gmail.com',
            'password' => bcrypt('reza'),
            'role' => 'Pelanggan',
            'status' => 'Aktif',
        ]);
    }
}
