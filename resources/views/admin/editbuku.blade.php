@extends('admin.layout.main')

@section('title', 'Nusantara - Tambah Kategori Buku')

@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Edit Buku</h1>
    <p class="mb-4">Administrator dapat mengedit dan mengupdate data buku jika ada kesalahan pada data buku.</p>
    
    <div class="row justify-content-center">
        <div class="col-lg-8 text-left">
            <div class="card border-primary">
                <div class="card-header bg-primary text-white">Edit Data Buku</div>
                <div class="card-body text-left">
                    <form action="{{ route('update-buku', $buku->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="nama">Nama Buku:</label>
                            <input type="text" class="form-control" id="nama_buku" placeholder="Masukkan Nama Buku" name="nama_buku" value="{{ old('nama_buku', $buku->nama_buku) }}">
                            @error('nama_buku')
                                <small>{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="nama">Gambar Buku:</label>
                            <input type="file" class="form-control" id="gambar_buku" placeholder="Masukkan Gambar" name="gambar_buku">
                            @error('gambar_buku')
                                <small>{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="kategori_buku">Kategori Buku:</label>
                            <select class="form-control" id="id_kategori_buku" name="id_kategori_buku">
                                <option value="">Pilih Kategori</option>
                                @foreach($kategori_buku as $kategori)
                                    <option value="{{ $kategori->id }}" {{ old('id_kategori_buku', $buku->id_kategori_buku) == $kategori->id ? 'selected' : '' }}>
                                        {{ $kategori->nama_kategori }}
                                    </option>
                                @endforeach
                            </select>
                            @error('id_kategori_buku')
                                <small>{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="nama">Penerbit:</label>
                            <input type="text" class="form-control" id="penerbit" placeholder="Masukkan Nama Penerbit" name="penerbit" value="{{ old('penerbit', $buku->penerbit) }}">
                            @error('penerbit')
                                <small>{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="nama">Pengarang:</label>
                            <input type="text" class="form-control" id="pengarang" placeholder="Masukkan Nama Pengarang" name="pengarang" value="{{ old('pengarang', $buku->pengarang) }}">
                            @error('pengarang')
                                <small>{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="deskripsi">Deskripsi:</label>
                            <textarea class="form-control" id="deskripsi" placeholder="Masukkan Deskripsi" name="deskripsi" rows="4">{{ old('deskripsi', $buku->deskripsi) }}</textarea>
                            @error('deskripsi')
                                <small>{{ $message }}</small>
                            @enderror
                        </div>                        
                        <div class="form-group">
                            <label for="nama">Tanggal Terbit:</label>
                            <input type="date" class="form-control" id="tanggal_terbit" placeholder="Masukkan Tanggal Terbit" name="tanggal_terbit" value="{{ old('tanggal_terbit', $buku->tanggal_terbit) }}">
                            @error('tanggal_terbit')
                                <small>{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="nama">Stok:</label>
                            <input type="number" class="form-control" id="stok" placeholder="Masukkan Jumlah Stok" name="stok" value="{{ old('stok', $buku->stok) }}">
                            @error('stok')
                                <small>{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="nama">Harga:</label>
                            <input type="number" class="form-control" id="harga" placeholder="Masukkan Harga" name="harga" value="{{ old('harga', $buku->harga) }}">
                            @error('harga')
                                <small>{{ $message }}</small>
                            @enderror
                        </div>
                        <a href="{{ route('buku') }}"><button type="button" class="btn btn-secondary">Kembali</button></a>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection