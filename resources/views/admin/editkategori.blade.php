@extends('admin.layout.main')

@section('title', 'Nusantara - Tambah Kategori Buku')

@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Edit Kategori Buku</h1>
    <p class="mb-4">Administrator dapat mengedit kategori buku untuk mengelompokkan buku-buku yang ada berdasarkan ketersediaan buku.</p>
    
    <div class="row justify-content-center">
        <div class="col-lg-8 text-left">
            <div class="card border-primary">
                <div class="card-header bg-primary text-white">Edit Data Kategori</div>
                <div class="card-body text-left">
                    <form action="{{ route('update-kategori', $kategori->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="nama">Nama Kategori:</label>
                            <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama Kategori" name="nama_kategori" value="{{ $kategori->nama_kategori }}">
                            @error('nama_kategori')
                                <small>{{ $message }}</small>
                            @enderror
                        </div>
                        <a href="{{ route('kategori-buku') }}"><button type="button" class="btn btn-secondary">Kembali</button></a>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection