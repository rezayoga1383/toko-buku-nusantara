@extends('admin.layout.main')

@section('title', 'Nusantara - Tambah Kategori Buku')

@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Edit Data Pengguna</h1>
    <p class="mb-4">Administrator dapat memperbarui data pengguna jika ada kesalahan</p>
    
    <div class="row justify-content-center">
        <div class="col-lg-8 text-left">
            <div class="card border-primary">
                <div class="card-header bg-primary text-white">Edit Data Pengguna</div>
                <div class="card-body text-left">
                    <form action="{{ route('update-pengguna', $pengguna->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="nama">Nama:</label>
                            <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama Pengguna" name="nama" value="{{ $pengguna->nama }}">
                            @error('nama')
                                <small>{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="nama">Email:</label>
                            <input type="email" class="form-control" id="email" placeholder="Masukkan Email Pengguna" name="email" value="{{ $pengguna->email }}">
                            @error('email')
                                <small>{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="nama">Password:</label>
                            <input type="password" class="form-control" id="password" placeholder="Masukkan Password Pengguna" name="password">
                            @error('password')
                                <small>{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="role">Role:</label>
                            <select class="form-control" id="role" name="role">
                                <option value="" disabled {{ old('role', $pengguna->role) == '' ? 'selected' : '' }}>Pilih Role Pengguna</option>
                                <option value="Admin" {{ old('role', $pengguna->role) == 'Admin' ? 'selected' : '' }}>Admin</option>
                                <option value="Pelanggan" {{ old('role', $pengguna->role) == 'Pelanggan' ? 'selected' : '' }}>Pelanggan</option>
                            </select>
                            @error('role')
                                <small>{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="status">Status:</label>
                            <select class="form-control" id="status" name="status">
                                <option value="" disabled {{ old('status', $pengguna->status) == '' ? 'selected' : '' }}>Pilih Status Pengguna</option>
                                <option value="Aktif" {{ old('status', $pengguna->status) == 'Aktif' ? 'selected' : '' }}>Aktif</option>
                                <option value="Tidak Aktif" {{ old('status', $pengguna->status) == 'Tidak Aktif' ? 'selected' : '' }}>Tidak Aktif</option>
                            </select>
                            @error('status')
                                <small>{{ $message }}</small>
                            @enderror
                        </div>
                        <a href="{{ route('pengguna') }}"><button type="button" class="btn btn-secondary">Kembali</button></a>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection