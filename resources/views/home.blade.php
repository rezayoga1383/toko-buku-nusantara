<!DOCTYPE html>
<html lang="en">

<head>
    <title>Nusantara | Home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- <link rel="apple-touch-icon" href="{{ asset('template/assets/img/apple-icon.png') }}"> --}}
    {{-- <link rel="shortcut icon" type="image/x-icon" href="{{ asset('template/assets/img/favicon.ico') }}"> --}}

    <link rel="stylesheet" href="{{ asset('template/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/assets/css/templatemo.css') }}">
    <link rel="stylesheet" href="{{ asset('template/assets/css/custom.css') }}">

    <!-- Load fonts style after rendering the layout styles -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
    <link rel="stylesheet" href="{{ asset('template/assets/css/fontawesome.min.css') }}">
</head>

<body>
    <!-- Header -->
    <nav class="navbar navbar-expand-lg navbar-light shadow">
        <div class="container d-flex justify-content-between align-items-center">

            <a class="navbar-brand text-success logo h1 align-self-center" href="/">
                Nusantara
            </a>

            <button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#templatemo_main_nav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="align-self-center collapse navbar-collapse flex-fill  d-lg-flex justify-content-lg-between" id="templatemo_main_nav">
                <div class="flex-fill">
                    <ul class="nav navbar-nav d-flex justify-content-center mx-lg-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="/">Beranda</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/about">Tentang</a>
                        </li>
                    </ul>
                </div>
                <div class="navbar align-self-center d-flex">
                    <a class="btn btn-custom text-decoration-none" href="/login">
                        <i class="fa fa-fw fa-user mr-2"></i> Login
                    </a>
                </div>
            </div>

        </div>
    </nav>

    <!-- Start Banner Hero -->
    <div id="template-mo-zay-hero-carousel" class="carousel slide" data-bs-ride="carousel">
        <ol class="carousel-indicators">
            <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="0" class="active"></li>
            <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="1"></li>
            <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="container">
                    <div class="row p-5">
                        <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                            <img class="img-fluid" src=" {{ asset('template./assets/img/1.png') }}" alt="">
                        </div>
                        <div class="col-lg-6 mb-0 d-flex align-items-center">
                            <div class="text-align-left align-self-center">
                                <h1 class="h1 text-success"><b>Toko Buku Nusantara</b></h1>
                                {{-- <h3 class="h2">Nusantara</h3> --}}
                                <p>
                                    Selamat datang di Toko Buku Nusantara, Kami adalah toko buku online yang menyediakan beragam koleksi buku dari berbagai genre, termasuk fiksi, non-fiksi, biografi, anak-anak, pendidikan, dan masih banyak lagi.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="container">
                    <div class="row p-5">
                        <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                            <img class="img-fluid" src="{{ asset('template/./assets/img/3.png') }}" alt="">
                        </div>
                        <div class="col-lg-6 mb-0 d-flex align-items-center">
                            <div class="text-align-left">
                                <h1 class="h1 text-success"><b>Harga Terjangkau</b></h1>
                                {{-- <h3 class="h2">Aliquip ex ea commodo consequat</h3> --}}
                                <p>
                                    Toko Nusantara selalu memastikan bahwa setiap buku yang dijual memiliki harga yang kompetitif, sehingga dapat diakses oleh semua orang. Selain itu, kami juga memberikan berbagai promo dan diskon menarik untuk memastikan pelanggan mendapatkan nilai lebih dari setiap pembelian.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="container">
                    <div class="row p-5">
                        <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                            <img class="img-fluid" src=" {{ asset('template/./assets/img/2.png') }}" alt="">
                        </div>
                        <div class="col-lg-6 mb-0 d-flex align-items-center">
                            <div class="text-align-left">
                                <h1 class="h1 text-success"><b>Pengiriman Cepat</b></h1>
                                {{-- <h3 class="h2">Ullamco laboris nisi ut </h3> --}}
                                <p>
                                    Toko Nusantara adalah toko buku yang menawarkan layanan pengiriman yang cepat dan handal, memastikan bahwa buku yang anda pesan tiba di tangan anda dengan aman dan tepat waktu. 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev text-decoration-none w-auto ps-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="prev">
            <i class="fas fa-chevron-left"></i>
        </a>
        <a class="carousel-control-next text-decoration-none w-auto pe-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="next">
            <i class="fas fa-chevron-right"></i>
        </a>
    </div>
    <!-- End Banner Hero -->


    <!-- Start Categories of The Month -->
    <section class="container py-5">
        <div class="row text-center pt-3">
            <div class="col-lg-6 m-auto">
                <h1 class="h1">Kategori</h1>
                <p>
                    Berikut Kategori Buku Yang Kami Sediakan 
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-4 p-3 mt-3">
                <a href="#"><img src="{{ asset('template/./assets/img/pendidikan.png') }}" class="rounded-circle img-fluid border"></a>
                <h5 class="text-center mt-3 mb-3">Pendidikan</h5>
                {{-- <p class="text-center"><a class="btn btn-outline-success">Selengkapnya</a></p> --}}
            </div>
            <div class="col-12 col-md-4 p-3 mt-3">
                <a href="#"><img src="{{ asset('template/./assets/img/agama.png') }}" class="rounded-circle img-fluid border"></a>
                <h2 class="h5 text-center mt-3 mb-3">Agama</h2>
                {{-- <p class="text-center"><a class="btn btn-outline-success">Selengkapnya</a></p> --}}
            </div>
            <div class="col-12 col-md-4 p-3 mt-3">
                <a href="#"><img src="{{ asset('template/./assets/img/fiksi.png') }}" class="rounded-circle img-fluid border"></a>
                <h2 class="h5 text-center mt-3 mb-3">Fiksi</h2>
                {{-- <p class="text-center"><a class="btn btn-outline-success">Selengkapnya</a></p> --}}
            </div>
            <div class="col-12 p-3 mt-3 d-flex justify-content-center">
                <p class="text-center"><a href="#" class="btn btn-outline-success">Lihat Semua</a></p>
            </div>
        </div>
    </section>
    <!-- End Categories of The Month -->

    <!-- Start Footer -->
    <footer class="bg-dark" id="tempaltemo_footer">
        <div class="container">
            <div class="row">

                <div class="col-md-4 pt-5">
                    <h2 class="h2 text-success border-bottom pb-3 border-light logo">Toko Nusantara</h2>
                    <ul class="list-unstyled text-light footer-link-list">
                        <li>
                            <i class="fas fa-map-marker-alt fa-fw"></i>
                            Jl. Dr. Wahidin No.1, Kepatihan, Kec. Bojonegoro, Kab. Bojonegoro, Jawa Timur 62111
                        </li>
                        <li>
                            <i class="fa fa-phone fa-fw"></i>
                            <a class="text-decoration-none" href="#">0877-6268-4812</a>
                        </li>
                        <li>
                            <i class="fa fa-envelope fa-fw"></i>
                            <a class="text-decoration-none" href="mailto:info@company.com">nusantara@gmail.com</a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-4 pt-5">
                    <h2 class="h2 text-light border-bottom pb-3 border-light">Kategori</h2>
                    <ul class="list-unstyled text-light footer-link-list">
                        <li><a class="text-decoration-none" href="#">Pendidikan</a></li>
                        <li><a class="text-decoration-none" href="#">Agama</a></li>
                        <li><a class="text-decoration-none" href="#">Fiksi</a></li>
                        {{-- <li><a class="text-decoration-none" href="#">Women's Shoes</a></li> --}}
                        {{-- <li><a class="text-decoration-none" href="#">Popular Dress</a></li> --}}
                        {{-- <li><a class="text-decoration-none" href="#">Gym Accessories</a></li> --}}
                        {{-- <li><a class="text-decoration-none" href="#">Sport Shoes</a></li> --}}
                    </ul>
                </div>

                <div class="col-md-4 pt-5">
                    <h2 class="h2 text-light border-bottom pb-3 border-light">Informasi</h2>
                    <ul class="list-unstyled text-light footer-link-list">
                        <li><a class="text-decoration-none" href="/">Beranda</a></li>
                        <li><a class="text-decoration-none" href="/about">Tentang</a></li>
                        {{-- <li><a class="text-decoration-none" href="#">Shop Locations</a></li> --}}
                        {{-- <li><a class="text-decoration-none" href="#">FAQs</a></li> --}}
                        {{-- <li><a class="text-decoration-none" href="#">Contact</a></li> --}}
                    </ul>
                </div>

            </div>
    </footer>
    <!-- End Footer -->

    <!-- Start Script -->
    <script src="{{ asset('template/assets/js/jquery-1.11.0.min.js') }}"></script>
    <script src="{{ asset('template/assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
    <script src="{{ asset('template/assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('template/assets/js/templatemo.js') }}"></script>
    <script src="{{ asset('template/assets/js/custom.js') }}"></script>
    <!-- End Script -->
</body>

</html>