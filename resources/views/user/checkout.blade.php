@extends('user.layout.main')

@section('title', 'Nusantara - Beranda')

@section('content')
<div class="container mt-3">
  <h2 class="text-success mb-3">Detail Pesanan</h2>
  <table class="table table-borderless">
    <tr>
      <td>Nama</td>
      <td>: {{ $order->nama }}</td>
    </tr>
    <tr>
      <td>No Hp</td>
      <td>: {{ $order->no_telepon }}</td>
    </tr>
    <tr>
      <td>Alamat</td>
      <td>: {{ $order->alamat }}</td>
    </tr>
    <tr>
      <td>Jumlah</td>
      <td>: {{ $order->jumlah }}</td>
    </tr>
    <tr>
      <td>Total Bayar</td>
      <td>: Rp {{ number_format($order->total_bayar, 0, ',', '.') }}</td>
    </tr>
  </table>
  <button id="pay-button" class="btn btn-success mb-5">Bayar Sekarang</button>

  <!-- Snap Container -->
  <div id="snap-container"></div>
</div>

<script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{ config('midtrans.client_key') }}"></script>
<script type="text/javascript">
  document.getElementById('pay-button').addEventListener('click', function () {
    // Trigger snap popup. Replace TRANSACTION_TOKEN_HERE with your actual transaction token
    window.snap.pay('{{$snapToken}}', {
      onSuccess: function(result){
        alert("Payment successful!"); 
        console.log(result);
        // Add additional logic for successful payment here
        window.location.href = "/order-success"; // Redirect to success page
      },
      onPending: function(result){
        alert("Waiting for your payment!"); 
        console.log(result);
        // Add additional logic for pending payment here
      },
      onError: function(result){
        alert("Payment failed!"); 
        console.log(result);
        // Add additional logic for failed payment here
      },
      onClose: function(){
        alert('You closed the popup without finishing the payment');
      }
    });
  });
</script>
@endsection
