@extends('user.layout.main')

@section('title', 'Nusantara - Beranda')

@section('content')
<div id="template-mo-zay-hero-carousel" class="carousel slide" data-bs-ride="carousel">
    <ol class="carousel-indicators">
        <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="0" class="active"></li>
        <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="1"></li>
        <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <div class="container">
                <div class="row p-5">
                    <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                        <img class="img-fluid" src=" {{ asset('template/./assets/img/1.png') }}" alt="">
                    </div>
                    <div class="col-lg-6 mb-0 d-flex align-items-center">
                        <div class="text-align-left align-self-center">
                            <h1 class="h1 text-success"><b>Toko Buku Nusantara</b></h1>
                            {{-- <h3 class="h2">Nusantara</h3> --}}
                            <p>
                                Selamat datang di Toko Buku Nusantara, Kami adalah toko buku online yang menyediakan beragam koleksi buku dari berbagai genre, termasuk fiksi, non-fiksi, biografi, anak-anak, pendidikan, dan masih banyak lagi.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="container">
                <div class="row p-5">
                    <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                        <img class="img-fluid" src="{{ asset('template/./assets/img/3.png') }}" alt="">
                    </div>
                    <div class="col-lg-6 mb-0 d-flex align-items-center">
                        <div class="text-align-left">
                            <h1 class="h1 text-success"><b>Harga Terjangkau</b></h1>
                            {{-- <h3 class="h2">Aliquip ex ea commodo consequat</h3> --}}
                            <p>
                                Toko Nusantara selalu memastikan bahwa setiap buku yang dijual memiliki harga yang kompetitif, sehingga dapat diakses oleh semua orang. Selain itu, kami juga memberikan berbagai promo dan diskon menarik untuk memastikan pelanggan mendapatkan nilai lebih dari setiap pembelian.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="container">
                <div class="row p-5">
                    <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                        <img class="img-fluid" src=" {{ asset('template/./assets/img/2.png') }}" alt="">
                    </div>
                    <div class="col-lg-6 mb-0 d-flex align-items-center">
                        <div class="text-align-left">
                            <h1 class="h1 text-success"><b>Pengiriman Cepat</b></h1>
                            {{-- <h3 class="h2">Ullamco laboris nisi ut </h3> --}}
                            <p>
                                Toko Nusantara adalah toko buku yang menawarkan layanan pengiriman yang cepat dan handal, memastikan bahwa buku yang anda pesan tiba di tangan anda dengan aman dan tepat waktu. 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev text-decoration-none w-auto ps-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="prev">
        <i class="fas fa-chevron-left"></i>
    </a>
    <a class="carousel-control-next text-decoration-none w-auto pe-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="next">
        <i class="fas fa-chevron-right"></i>
    </a>
</div>
<section id="kategori" class="container py-5">
    <div class="row text-center pt-3">
        <div class="col-lg-6 m-auto">
            <h1 class="h1">Kategori</h1>
            <p>
                Berikut Kategori Buku Yang Kami Sediakan 
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-4 p-3 mt-3">
            <a href="#"><img src="{{ asset('template/./assets/img/pendidikan.png') }}" class="rounded-circle img-fluid border"></a>
            <h5 class="text-center mt-3 mb-3">Pendidikan</h5>
            {{-- <p class="text-center"><a class="btn btn-outline-success">Selengkapnya</a></p> --}}
        </div>
        <div class="col-12 col-md-4 p-3 mt-3">
            <a href="#"><img src="{{ asset('template/./assets/img/agama.png') }}" class="rounded-circle img-fluid border"></a>
            <h2 class="h5 text-center mt-3 mb-3">Agama</h2>
            {{-- <p class="text-center"><a class="btn btn-outline-success">Selengkapnya</a></p> --}}
        </div>
        <div class="col-12 col-md-4 p-3 mt-3">
            <a href="#"><img src="{{ asset('template/./assets/img/fiksi.png') }}" class="rounded-circle img-fluid border"></a>
            <h2 class="h5 text-center mt-3 mb-3">Fiksi</h2>
            {{-- <p class="text-center"><a class="btn btn-outline-success">Selengkapnya</a></p> --}}
        </div>
        <div class="col-12 p-3 mt-3 d-flex justify-content-center">
            <p class="text-center"><a href="#" class="btn btn-outline-success">Lihat Semua</a></p>
        </div>
    </div>
</section>
@endsection