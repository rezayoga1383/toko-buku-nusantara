<div class="container">
    <div class="row">

        <div class="col-md-4 pt-5">
            <h2 class="h2 text-success border-bottom pb-3 border-light logo">Toko Nusantara</h2>
            <ul class="list-unstyled text-light footer-link-list">
                <li>
                    <i class="fas fa-map-marker-alt fa-fw"></i>
                    Jl. Dr. Wahidin No.1, Kepatihan, Kec. Bojonegoro, Kab. Bojonegoro, Jawa Timur 62111
                </li>
                <li>
                    <i class="fa fa-phone fa-fw"></i>
                    <a class="text-decoration-none" href="#">0877-6268-4812</a>
                </li>
                <li>
                    <i class="fa fa-envelope fa-fw"></i>
                    <a class="text-decoration-none" href="mailto:info@company.com">nusantara@gmail.com</a>
                </li>
            </ul>
        </div>

        <div class="col-md-4 pt-5">
            <h2 class="h2 text-light border-bottom pb-3 border-light">Kategori</h2>
            <ul class="list-unstyled text-light footer-link-list">
                <li><a class="text-decoration-none" href="#">Pendidikan</a></li>
                <li><a class="text-decoration-none" href="#">Agama</a></li>
                <li><a class="text-decoration-none" href="#">Fiksi</a></li>
            </ul>
        </div>

        <div class="col-md-4 pt-5">
            <h2 class="h2 text-light border-bottom pb-3 border-light">Informasi</h2>
            <ul class="list-unstyled text-light footer-link-list">
                <li><a class="text-decoration-none" href="/">Beranda</a></li>
                <li><a class="text-decoration-none" href="/about">Tentang</a></li>
            </ul>
        </div>

    </div>