<!DOCTYPE html>
<html lang="en">

<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- <link rel="apple-touch-icon" href="{{ asset('template/assets/img/apple-icon.png') }}"> --}}
    {{-- <link rel="shortcut icon" type="image/x-icon" href="{{ asset('template/assets/img/favicon.ico') }}"> --}}

    @include('user.layout.style')
</head>

<body>
    <!-- Header -->
    @include('user.layout.navbar')

    {{-- content --}}
    <section class="section">
        @yield('content')
    </section>
    {{-- end content --}}

    <!-- Start Footer -->
    <footer class="bg-dark" id="tempaltemo_footer">
        @include('user.layout.footer')
    </footer>
    <!-- End Footer -->

    @include('user.layout.script')
</body>

</html>