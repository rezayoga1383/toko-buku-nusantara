<nav class="navbar navbar-expand-lg navbar-light shadow">
    <div class="container d-flex justify-content-between align-items-center">

        <a class="navbar-brand text-success logo h1 align-self-center" href="/user/beranda">
            Nusantara
        </a>

        <button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#templatemo_main_nav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="align-self-center collapse navbar-collapse flex-fill  d-lg-flex justify-content-lg-between" id="templatemo_main_nav">
            <div class="flex-fill">
                <ul class="nav navbar-nav d-flex justify-content-center mx-lg-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/user/beranda">Beranda</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/user/beranda/#kategori">Kategori</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/user/produk">Produk</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/user/about">Tentang</a>
                    </li>
                </ul>
            </div>
            <div class="navbar align-self-center d-flex">
                <div class="dropdown">
                    <a class="btn btn-custom text-decoration-none dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-fw fa-user mr-2"></i> {{ auth()->user()->nama }}
                    </a>
            
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <form action="{{ route('logout') }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-transparent text-dark">
                                Logout
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</nav>