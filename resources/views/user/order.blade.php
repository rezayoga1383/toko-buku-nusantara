@extends('user.layout.main')

@section('title', 'Nusantara - Beranda')

@section('content')
<div class="container mt-5">
    <h2 class="text-success">Form Pemesanan</h2>
    <form action="/user/checkout" method="POST">
      @csrf
      <div class="mb-3 mt-3">
        <label for="Quantity">Quantity</label>
        <input type="number" class="form-control" id="jumlah" placeholder="Masukkan jumlah yang mau dipesan" name="jumlah">
      </div>
      <div class="mb-3">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama Anda" name="nama">
      </div>
      <div class="mb-3">
        <label for="notelepon">No Telp</label>
        <input type="text" class="form-control" id="no_telepon" placeholder="Masukkan Nama Anda" name="no_telepon">
      </div>
      <div class="mb-3">
        <label for="alamat">Alamat</label>
        <textarea name="alamat" class="form-control" id="alamat" cols="30" rows="10"></textarea>
      </div>
      <button type="submit" class="btn btn-success mt-2 mb-5">Submit</button>
    </form>
  </div>
@endsection