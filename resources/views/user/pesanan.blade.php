<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Detail Pesanan</title>
  <!-- Bootstrap CSS -->
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container mt-5">
  <div class="card text-white bg-success mb-3" style="max-width: 18rem;">
    <div class="card-header">Detail Pesanan</div>
    <div class="card-body">
      <h5 class="card-title">ID Pesanan: #{{ $order->id }}</h5>
      <p class="card-text">Total Harga: Rp {{ number_format($order->total_bayar, 0, ',', '.') }}</p>
      <p class="card-text">Jumlah: {{ $order->jumlah }}</p>
    </div>
  </div>
</div>

<!-- Bootstrap JS and dependencies -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.2/dist/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
