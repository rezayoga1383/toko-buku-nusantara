<?php

use App\Models\Order;
use App\Models\Pengguna;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BukuController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\PenggunaController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\KategoriBukuController;
use App\Http\Controllers\ProdukDetailController;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });
// Route::get('/about', function () {
//     return view('about');
// });
// Route::get('/login', function () {
//     return view('login');
// });
// Route::get('/dashboard-admin', function () {
//     return view('admin.dashboard');
// });
// Route::get('/beranda-user', function () {
//     return view('user.home');
// });
// Route::get('/about-user', function () {
//     return view('user.about');
// });
// Register
Route::get('/register', [RegisterController::class, 'index'])->name('register');
Route::post('/daftar', [RegisterController::class, 'daftar'])->name('daftar');


// Login Pelanggan
Route::get('/', [LoginController::class, 'index']);
Route::get('/about', function () {
    return view('about');
    });
Route::get('/login', [LoginController::class, 'login'])->name('login');
Route::post('/autentikasi', [LoginController::class, 'autentikasi'])->name('autentikasi');

Route::get('/user/beranda', [LoginController::class, 'pelanggan'])->name('beranda-user');
Route::get('/user/about', function () {
    return view('user.about');
    });
Route::get('/user/produk', [ProdukController::class, 'index'])->name('produk-user');
Route::get('/user/produk-detail/{id}', [ProdukDetailController::class, 'index'])->name('produk-detail-user');

// Payment
Route::get('/user/pemesanan', [OrderController::class, 'index'])->name('order-produk');
Route::post('/user/checkout', [OrderController::class, 'checkout'])->name('checkout-produk');

// Login Admin            
Route::get('/admin/dashboard', [LoginController::class, 'admin'])->name('dashboard-admin');
Route::get('/admin/dashboard', [DashboardController::class, 'index'])->name('dashboard-admin');
Route::get('/admin/kategori-buku', [KategoriBukuController::class, 'index'])->name('kategori-buku');

// Kategori Buku
Route::get('/admin/kategori-buku/create', [KategoriBukuController::class, 'create'])->name('tambah-kategori');
Route::post('/admin/kategori-buku/store', [KategoriBukuController::class, 'store'])->name('create-kategori');
Route::get('/kategori_buku/edit/{id}', [KategoriBukuController::class, 'edit'])->name('edit-kategori');
Route::put('/kategori_buku/update/{id}', [KategoriBukuController::class, 'update'])->name('update-kategori');
Route::delete('/kategori_buku/destroy/{id}', [KategoriBukuController::class, 'destroy'])->name('delete-kategori');

// Buku
Route::get('/admin/buku', [BukuController::class, 'index'])->name('buku');
Route::get('/admin/buku/create', [BukuController::class, 'create'])->name('tambah-buku');
Route::post('/admin/buku/store', [BukuController::class, 'store'])->name('create-buku');
Route::get('/buku/edit/{id}', [BukuController::class, 'edit'])->name('edit-buku');
Route::put('/buku/{id}', [BukuController::class, 'update'])->name('update-buku');
Route::delete('/buku/{id}', [BukuController::class, 'destroy'])->name('delete-buku');

// Pengguna
Route::get('admin/pengguna', [PenggunaController::class, 'index'])->name('pengguna');
Route::get('/admin/pengguna/create', [PenggunaController::class, 'create'])->name('tambah-pengguna');
Route::post('/admin/pengguna/store', [PenggunaController::class, 'store'])->name('create-pengguna');
Route::get('/pengguna/edit/{id}', [PenggunaController::class, 'edit'])->name('edit-pengguna');
Route::put('/pengguna/{id}', [PenggunaController::class, 'update'])->name('update-pengguna');
Route::delete('/pengguna/{id}', [PenggunaController::class, 'destroy'])->name('delete-pengguna');

// logout
Route::post('/logout', [LogoutController::class, 'logout'])->name('logout');